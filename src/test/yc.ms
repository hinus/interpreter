var fac : Any->Int->Int;
fac = $f:Any =>$n:Int =>1 if n == 1 else f(f)(n-1) * n;
var U : Any->Any;
U = $u:Any =>u(u);
println(U(fac)(5));

var fac0:(Int->Int)->Int->Int;
fac0 = $f:Int->Int =>$n:Int =>1 if n == 1 else n*f(n-1);
var Y = $f:Any => U($x:Any => f($v:Any => x(x)(v)));
println(Y(fac0)(3));
println(Y(fac0)(4));
println(Y(fac0)(5));
println(Y(fac0)(6));

