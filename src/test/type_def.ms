type MyInt = Int;
type Func = MyInt->MyInt;
var f:Func;
f = $x:MyInt => x + 1;
var d:Func = $x:MyInt => x * 2;
println(f(3));
println(d(3));

type CNum<T> = (T->T)->T->T;
type INum = CNum<Int>;
