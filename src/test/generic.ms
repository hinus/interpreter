type CNum<a> = (a->a)->a->a;
var i0 : CNum<Int>;
i0 = $f:Int->Int => $x:Int => x;
var isucc = $m:CNum<Int> => $f:Int->Int => $x:Int => f(m(f)(x));
var i1 = isucc(i0);

var s0: CNum<String>;
s0 = $f:String->String => $x:String => x;
var ssucc = $m:CNum<String> => $f:String->String => $x:String => f(m(f)(x));
var s1 = ssucc(s0);
var add_a => $x:String : x + "a";
println(s1(add_a)(""));

# The Best:
var succ = poly<T>{$m:CNum<T> => $:T->T => $x:T => f(m(f)(x))};

