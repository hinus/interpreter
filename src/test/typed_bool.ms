var str_true = "true";
var str_false = "false";
type L_BOOL = String -> String -> String;
var lambda_true : L_BOOL;
var lambda_false : L_BOOL;
lambda_true = $x:String => $y:String => x;
lambda_false = $x:String => $y:String => y;
var ifthenelse = $x: L_BOOL => x;
var to_bool = $b: L_BOOL => b(str_true)(str_false);
type L_LOGICAL = (String->String->String)->(String->String->String)->(String->String->String);
var logical_true : L_LOGICAL;
var logical_false : L_LOGICAL;
logical_true = $x:L_BOOL => $y:L_BOOL => x;
logical_false = $x:L_BOOL => $y:L_BOOL => y;
var lambda_and = $x:L_LOGICAL => $y:L_BOOL => x(y)(lambda_false);
var lambda_or = $x:L_LOGICAL => $y:L_BOOL => x(lambda_true)(y);
var lambda_not = $x:L_LOGICAL => x(lambda_false)(lambda_true);
println(to_bool(lambda_true));
println(to_bool(lambda_false));
println(to_bool(lambda_and(logical_true)(lambda_true)))
println(to_bool(lambda_and(logical_true)(lambda_false)))
println(to_bool(lambda_and(logical_false)(lambda_true)))
println(to_bool(lambda_and(logical_false)(lambda_false)))
println(to_bool(lambda_or(logical_true)(lambda_true)))
println(to_bool(lambda_or(logical_true)(lambda_false)))
println(to_bool(lambda_or(logical_false)(lambda_true)))
println(to_bool(lambda_or(logical_false)(lambda_false)))
println(to_bool(lambda_not(logical_true)))
println(to_bool(lambda_not(logical_false)))
