#include "env.hpp"
#include "runtime/dict_object.hpp"


StackFrame::StackFrame(DictObject* env) {
    _var_table = new std::map<std::string, Object*>();
    env->copy_into_map(_var_table);
}

void StackFrame::save_var(const char* name, Object* value) {
    (*_var_table)[std::string(name)] = value;
}

StackFrame::~StackFrame() {
    if (_var_table) {
        _var_table->clear();
    }

    delete _var_table;
}

Object* StackFrame::lookup(const char* name) {
    auto t = _var_table->find(std::string(name));

    if (t != _var_table->end()) {
        return t->second;
    }
    else {
        return NULL;
    }
}

