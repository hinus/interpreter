#include "runtime/functionObject.hpp"
#include "runtime/klass.hpp"

#include <cstdio>

NativeFunctionObject::NativeFunctionObject(NativeFunc f) : _func(f) {
    set_klass(NativeFunctionKlass::get_instance());
}

void NativeFunctionObject::print() {
}

Object* sys_print(std::vector<Object*>& args) {
    args[0]->print();
    return UnitObject::UnitValue;
}

Object* sys_println(std::vector<Object*>& args) {
    args[0]->print();
    printf("\n");
    return UnitObject::UnitValue;
}

