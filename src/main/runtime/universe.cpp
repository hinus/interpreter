#include "type/type.hpp"
#include "runtime/heap.hpp"
#include "runtime/universe.hpp"

Space* Universe::type_space = NULL;

void Universe::genesis() {
    type_space = new Space(1*1024*1024);
}

void Universe::destroy() {
    delete Heap::get_instance();
}

void Universe::clear_type_space() {
    delete type_space;
}

