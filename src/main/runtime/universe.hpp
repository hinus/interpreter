#ifndef UNIVERSE_HPP
#define UNIVERSE_HPP

class Space;

class Universe {
public:
    static void genesis();
    static void destroy();

    static Space* type_space;
    
    static void clear_type_space();
};

#endif
