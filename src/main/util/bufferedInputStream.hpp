#ifndef BUFFERED_INPUT_STREAM_HPP
#define BUFFERED_INPUT_STREAM_HPP

#include <stdio.h>
#include <string.h>

#define BUFFER_LEN 256

class BufferedInputStream {
private:
    FILE* fp;
    char szBuffer[BUFFER_LEN];
    unsigned short index;

public:
    BufferedInputStream(char const* filename) {
        fp = fopen(filename, "rb");
        memset(szBuffer, 0, BUFFER_LEN * sizeof(char));
        fread(szBuffer, BUFFER_LEN * sizeof(char), 1, fp);
        index = 0;
    }

    ~BufferedInputStream() {
        close();
    }

    char read() {
        char c = peek();
        index++;
        return c;
    }

    int read_int() {
        int b1 = read() & 0xff;
        int b2 = read() & 0xff;
        int b3 = read() & 0xff;
        int b4 = read() & 0xff;

        return b4 << 24 | b3 << 16 | b2 << 8 | b1;
    }

    char peek() {
        if (index < BUFFER_LEN)
            return szBuffer[index];
        else {
            index = 0;
            memset(szBuffer, 0, BUFFER_LEN * sizeof(char));
            fread(szBuffer, BUFFER_LEN * sizeof(char), 1, fp);
            return szBuffer[index];
        }
    }

    void advance() {
        index++;
    }

    double read_double() {
        char t[8];
        for (int i = 0; i < 8; i++) {
            t[i] = read();
        }

        return *(double *)t;
    }

    void close() {
        if (fp != NULL) {
            fclose(fp);
            fp = NULL;
        }
    }
};

#endif
